const express = require("express");
const { fstat, createReadStream } = require("fs");
const jwt = require("jsonwebtoken");
const { GridFSBucket } = require("mongodb");

// recordRoutes is an instance of the express router.
// We use it to define our routes.
// The router will be added as a middleware and will take control of requests starting with path /record.
const recordRoutes = express.Router();

// This will help us connect to the database
const dbo = require("../db/conn");

// This help convert the id from string to ObjectId for the _id.
const ObjectId = require("mongodb").ObjectId;


recordRoutes.route("/updateBox").post(getUserInfo, function (req, res) {
    let db_connect = dbo.getDb();
    
    const box = {
        _id: ObjectId(req.body._id),
        name: req.body.name,
        description: req.body.description,
        public: req.body.public,
        file_name: req.body.file ? req.body.file.name : null,
        file_base64: req.body.file ? req.body.file.base64 : null,
        sharedWith: req.body.sharedWith ? req.body.sharedWith.toLowerCase().split("\n") : null,
    };

    db_connect.collection("boxes").updateOne({ _id: box._id }, { $set: box }, function (err, result) {
        if (err) throw err;

        res.json({ status: "Box Updated" });
    });
})

recordRoutes.route("/deleteBox").post(getUserInfo, async function (req, res) {
    let db_connect = dbo.getDb();
    var box;

    // Uses the decoded token to make sure they are the owner of the box
    // If the token is not valid, or they arent a superUser, they will not be able to delete the box
    if (req.decoded.superUser) {
        box = {
            _id: new ObjectId(req.body.id),
        };
    }
    else {
        box = {
            owner_id: req.decoded.id,
            _id: new ObjectId(req.body.id),
        };
    }


    if (db_connect) {
        db_connect.collection("boxes").deleteOne(box, function (err, result) {
            if (err) {
                res.json({ status: "Error" });
            }
        });
    }

    res.json({ status: "Box Deleted" });
})

recordRoutes.route("/createBox").post(getUserInfo, function (req, response) {
    let db_connect = dbo.getDb();

    // We can access user data with req.decoded.id. Attach owners ID to the box
    let myobj = {
        owner_id: req.decoded ? req.decoded.id : "Guest",
        name: req.body.name,
        description: req.body.description,
        public: req.decoded ? false : true, // Public if they are not logged in
        file_name: req.body.file ? req.body.file.name : null,
        file_base64: req.body.file ? req.body.file.base64 : null,
        sharedWith: '',
    };

    db_connect.collection("boxes").insertOne(myobj, function (err, res) {
        if (err) throw err;
        if (res.acknowledged)
            response.json({ status: "Box Created" });

    });

});

recordRoutes.route("/getOwnedBoxes").get(getUserInfo, function (req, response) {
    let db_connect = dbo.getDb();

    // Get all boxes owned by the user
    if (req.decoded && db_connect) {
        db_connect.collection("boxes").find({ owner_id: req.decoded.id }).toArray(function (err, result) {
            if (err) throw err;

            response.json(result);
        });
    }
});

recordRoutes.route("/getSharedBoxes").get(getUserInfo, function (req, response) {
    let db_connect = dbo.getDb();

    // Get all boxes where the sharedWith array contains the user name
    if (req.decoded && db_connect) {
        db_connect.collection("boxes").find({ sharedWith: { $regex: req.decoded.name }, owner_id: { $ne: req.decoded.id } }).toArray(function (err, result) {
            if (err) throw err;

            response.status(200).json({ result: result, superUser: req.decoded ? req.decoded.superUser : false });
        });
    }
})

recordRoutes.route("/getPublicBoxes").get(getUserInfo, function (req, response) {
    let db_connect = dbo.getDb();

    // Get all public boxes where the owner is not the current user  
    if (db_connect) {
        db_connect.collection("boxes").find({ public: true, owner_id: { $ne: req.decoded ? req.decoded.id : null } }).toArray(function (err, result) {
            if (err) throw err;

            // Success, pass through data and users superUser status
            response.status(200).json({ result: result, superUser: req.decoded ? req.decoded.superUser : false });
        });
    }
})

recordRoutes.route("/getAllBoxes").get(getUserInfo, function (req, response) {
    let db_connect = dbo.getDb();


    if (req.decoded) { // Must check decoded exists before accessing .superUser
        if (db_connect && req.decoded.superUser) {
            // The user must be a super user to get all boxes
            db_connect.collection("boxes").find({}).toArray(function (err, result) {
                if (err) throw err;

                response.status(200).json({ result: result, superUser: req.decoded.superUser });
            });
        }
    }

})
// This will get the users information from the token stored on there browser
function getUserInfo(req, res, next) {
    const token = req.headers["x-access-token"];

    jwt.verify(token, process.env.SECRET, function (err, decoded) {
        req.decoded = decoded;

        // Continue to next function with the user data (can be null)
        next();
    });
}

module.exports = recordRoutes;