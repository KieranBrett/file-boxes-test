const express = require("express");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

// recordRoutes is an instance of the express router.
// We use it to define our routes.
// The router will be added as a middleware and will take control of requests starting with path /record.
const recordRoutes = express.Router();

// This will help us connect to the database
const dbo = require("../db/conn");
const e = require("express");

// This help convert the id from string to ObjectId for the _id.
const ObjectId = require("mongodb").ObjectId;

recordRoutes.route("/register").post(async function (req, res) {
    let db_connect = dbo.getDb();

    const user = {
        name: req.body.name,
        password: req.body.password,
        superUser: false
    };

    // Check if username/email is taken
    const takenName = await db_connect.collection("users").findOne({ name: user.name });

    if (takenName != null) {
        res.json({ status: "Email/Username already taken" });
    } else {
        user.password = await bcrypt.hash(user.password, 10);

        db_connect.collection("users").insertOne(user, function (err, result) { // Insert User Entry
            if (err) throw err;
        });
        res.json({ status: "Account Created" });
    }
})

recordRoutes.route("/login").post(async function (req, res) {
    let db_connect = dbo.getDb();

    const user = {
        name: req.body.name,
        password: req.body.password,
    }

    db_connect.collection("users").findOne({ name: user.name }, function (err, result) {
        // If the user exists, compare there attempt against the database
        if (result != null) {
            bcrypt.compare(user.password, result.password, function (err, cryptResult) {
                if (cryptResult) {
                    // If right, Issue a token that will expire in 1 hour 
                    const token = jwt.sign({ id: result._id, name: user.name, superUser: result.superUser }, process.env.SECRET, { expiresIn: "1h" });
                    res.json({ status: "Logged In", token: token });
                } else {
                    // Abillity to have a seperate message for failed Password
                    // But it would be a security flaw, letting people quickly see if a user exists
                    return res.json({ status: "Invalid Username or Password" })
                }
            });
        }
        else {
            return res.json({ status: "Invalid Username or Password" })
        }
    })
})

recordRoutes.route("/getUser").get(verifyJWT, function (req, res) {
    // console.log(req.decoded) // Testing purposes
    res.json({ status: "User Found", user: req.decoded.id });
})

// We only need to verify the JWT token before we write/read from the database
// For the front end we can simply check local storage to see if a token is present
// If it is, we can assume logged in and show the logged in UI
function verifyJWT(req, res, next) {
    const token = req.headers["x-access-token"];

    // localStorage.getItem returns "null" string instead of null if no token is present
    if (token != "null") {
        jwt.verify(token, process.env.SECRET, function (err, decoded) {
            if (err) {
                return res.json({ status: "Invalid Token" });
            } else {
                req.decoded = decoded;
                // console.log(req.decoded) // Testing: Log out the decoded token
                next();
            }
        });
    }
}

module.exports = recordRoutes;