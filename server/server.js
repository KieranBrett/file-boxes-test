const express = require("express");
const app = express();
const cors = require("cors");

const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const User = require("./models/user");

require("dotenv").config({ path: "./config.env" });

const port = process.env.PORT || 5000;

app.use(cors());
// Limit file size to 50mb (default is too small)
app.use(express.json({limit:'50mb'}));
app.use(express.json());

// Keeping the routes seperate is not nessecary for an application this small
// But it as good practice to keep them seperate, so that it could scale
// Add Authentication Routes
app.use(require("./routes/auth"));
// Add CRUD Routes for Item Boxes
app.use(require("./routes/crud"));

// get driver connection
const dbo = require("./db/conn");
 
app.listen(port, () => {
  // perform a database connection when server starts
  dbo.connectToServer(function (err) {
    if (err) console.error(err);
 
  });
  console.log(`Server is running on port: ${port}`);
});
