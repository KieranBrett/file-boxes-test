import './App.css';
import { useState, useEffect } from 'react';

import {
  BrowserRouter,
  Routes,
  Route
} from "react-router-dom";

// MUI
import Container from '@mui/material/Container';
import { Grid } from '@mui/material';

import NavBar from './components/NavBar'
import CreateBox from './components/crud-boxes/CreateBox';
import OwnedBox from './components/crud-boxes/OwnedBox';
import PublicBox from './components/crud-boxes/PublicBox';

function App() {
  return (
    <BrowserRouter>
      <NavBar />

      <Container maxWidth="md">

        <Routes>
          {/* One Page Web App at the moment, abillity to scale */}
          <Route path="/" element={<Home />} />
        </Routes>
      </Container>
    </BrowserRouter>
  );
}

function Home() {
  return <div style={{ padding: 30 }}>
    <CreateBox />
    <OwnedBoxes />
    <PublicBoxes />
    <SharedBoxes />
    <AllBoxes />
  </div>
}

function AllBoxes() {
  const [boxes, setBoxes] = useState([]);
  const [superUser, setSuperUser] = useState(false);

  useEffect(() => {
    fetch("http://localhost:5000/getAllBoxes", {
      method: "GET",
      headers: { "x-access-token": localStorage.getItem("token") }
    })
      .then(res => res.json())
      .then(data => {
        setSuperUser(data.superUser);
        setBoxes(data.result);
      })
      .catch()
  })

  if (superUser) {
    return <>
      <h1>All Boxes (Admin Panel)</h1>

      <Grid
        container
        spacing={2}
        direction="row"
        justify="flex-start"
        alignItems="flex-start">
        {boxes.map(box => <OwnedBox {...box} key={box._id} />)}
      </ Grid>
    </>
  }
  return null
}
// Get public boxes
function PublicBoxes() {
  const [boxes, setBoxes] = useState([]);
  const [superUser, setSuperUser] = useState(false);

  useEffect(() => {
    fetch("http://localhost:5000/getPublicBoxes", {
      method: "GET",
      headers: { "x-access-token": localStorage.getItem("token") }
    })
      .then(res => res.json())
      .then(data => {
        setSuperUser(data.superUser);
        setBoxes(data.result);
      })
      .catch()
  })

  return <>
    <h1>Public Boxes (Excludes personal boxes)</h1>

    {boxes.length === 0 ? <h4>No public boxes</h4> : null}

    <Grid
      style={{ padding: 30 }}
      container
      spacing={1}
      direction="row"
      justify="flex-start"
      alignItems="flex-start">
      {boxes.map(box => superUser ? <OwnedBox {...box} key={box._id} /> : <PublicBox {...box} key={box._id} />)}
    </ Grid>
  </>
}
// Get Shared boxes
function SharedBoxes() {
  const [boxes, setBoxes] = useState([]);
  const [superUser, setSuperUser] = useState(false);
  // If a token is present, the user is logged in
  const loggedIn = localStorage.getItem("token") !== null

  useEffect(() => {
    fetch("http://localhost:5000/getSharedBoxes", {
      method: "GET",
      headers: { "x-access-token": localStorage.getItem("token") }
    })
      .then(res => res.json())
      .then(data => {
        setSuperUser(data.superUser);
        setBoxes(data.result);
      })
      .catch()
  })

  if (loggedIn) {
    return <>
      <h1>Shared Boxes</h1>
      {boxes.length === 0 ? <h4>No boxes are being shared with you at the moment.</h4> : null}
      <Grid
        style={{ padding: 30 }}
        container
        spacing={1}
        direction="row"
        justify="flex-start"
        alignItems="flex-start">
        {boxes.map(box => superUser ? <OwnedBox {...box} key={box._id} /> : <PublicBox {...box} key={box._id} />)}
      </ Grid>
    </>
  }

  // Could add a sign in/register to share lists message here
  return null
}
// Get all boxes owned by the user
function OwnedBoxes() {
  const [boxes, setBoxes] = useState([]);
  // If a token is present, the user is logged in
  const loggedIn = localStorage.getItem("token") !== null

  useEffect(() => {
    fetch("http://localhost:5000/getOwnedBoxes", {
      method: "GET",
      headers: { "x-access-token": localStorage.getItem("token") }
    })
      .then(res => res.json())
      .then(data => {
        if (data !== boxes) {
          setBoxes(data);
        }
      })
      .catch()
  })

  if (loggedIn) {
    return <>
      <h1>My Boxes</h1>

      {boxes.length === 0 ? <h4>You have no boxes</h4> : null}
      <Grid
        container
        spacing={1}
        direction="row"
        justify="flex-start"
        alignItems="flex-start">
        {boxes.map(box => <OwnedBox {...box} key={box._id} />)}
      </ Grid>
    </>
  }

  // Could add a sign in to view boxes message here
  return null;

}

export default App;
