import { Link } from "react-router-dom";
import { useEffect, useState } from "react";

// MUI
import Button from '@mui/material/Button';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Box from '@mui/material/Box';
import Login from './user/Login';
import Typography from '@mui/material/Typography';

import HomeIcon from '@mui/icons-material/Home';
import IconButton from '@mui/material/IconButton';

const style = {
    background: 'white',
};

export default function NavBar() {
    const [loggedIn, setLoggedIn] = useState(false);

    // Better than just checking if a token is present in local storage
    // Because this confirms the identity of the user
    useEffect(() => {
        fetch("http://localhost:5000/getUser", {
            method: "GET",
            headers: {
                "x-access-token": localStorage.getItem("token"),
            }
        })
        .then(res => res.json())
        .then(data => {
            if (data.status === "User Found") {
                setLoggedIn(true);
            }
        })
        .catch(error => console.log(error));
    })

    return <Box sx={{ flexGrow: 1 }}>
        <AppBar style={style} position="static">
            <Toolbar>
                <Link to="/">
                    <IconButton>
                        <HomeIcon />
                    </IconButton>
                </Link>

                {/* Spaces buttons to the very right of the appbar*/}
                <Typography variant="h6" component="div" sx={{ flexGrow: 1 }} />
                
                { loggedIn ? <Button onClick={signOut}>Logout</Button> : <Login /> }
                
            </Toolbar>
        </AppBar>
    </Box>

    function signOut() {
        localStorage.removeItem("token");
        setLoggedIn(false);
        window.location.reload();
    }
}