import * as React from 'react';

// MUI Components
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import TextField from '@mui/material/TextField';

export default function Register() {
    const [open, setOpen] = React.useState(false);
    const [username, setUsername] = React.useState('');
    const [pass, setPass] = React.useState('');
    const [confirmPass, setConfirmPass] = React.useState('');
    const [error, setError] = React.useState('');

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    async function onSubmit(e) {
        // Basic data validation
        if (username.length < 5) {
            setError("Username/Email must be at least 5 characters long");
        }
        else if (pass.length < 5) {
            setError("Password must be at least 5 characters long");
        }
        else if (pass !== confirmPass) {
            setError("Passwords do not match");
        }
        else {
            await fetch("http://localhost:5000/register", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({ name: username.toLowerCase(), password: pass }),
            })
                .then(res => res.json())
                .then(data => {
                    if (data.status === "Account Created") {
                        setOpen(false);
                    }
                    else {
                        setError(data.status);
                    }
                }).catch(error => {
                    window.alert(error);
                    return;
                });
        }
    }

    return (
        <>
            <Button variant="outlined" onClick={handleClickOpen}>
                Register Here
            </Button>

            {/* Dialog that opens/hides when button is clicked */}
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">
                    Create your Account
                </DialogTitle>

                <DialogContent>
                    <DialogContentText>
                        Fill out the details to create your Account.
                    </DialogContentText>

                    <DialogContentText style={{ color: "#FF0000" }}>
                        {error ? error : null}
                    </DialogContentText>

                    <TextField
                        autoFocus
                        margin="dense"
                        id="name"
                        label="Email Address/Username"
                        type="email"
                        fullWidth
                        variant="standard"
                        // Set react state as true value
                        onChange={(e) => {
                            setError(null);
                            setUsername(e.target.value)
                        }}
                        value={username}
                    />

                    <TextField
                        autoFocus
                        margin="dense"
                        id="pass"
                        label="Password"
                        type="password"
                        fullWidth
                        variant="standard"
                        // Set react state as true value
                        onChange={(e) => {
                            setError(null);
                            setPass(e.target.value)
                        }}
                        value={pass}
                    />
                    <TextField
                        autoFocus
                        margin="dense"
                        id="passConfirm"
                        label="Confirm Password"
                        type="password"
                        fullWidth
                        variant="standard"
                        // Set react state as true value
                        onChange={(e) => {
                            setError(null);
                            setConfirmPass(e.target.value)
                        }}
                        value={confirmPass}
                    />
                </DialogContent>

                <DialogActions>
                    <Button onClick={handleClose}>Cancel</Button>
                    <Button onClick={onSubmit} autoFocus>
                        Register
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    );
}