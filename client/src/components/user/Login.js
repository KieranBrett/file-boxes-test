import * as React from 'react';

// MUI Components
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import TextField from '@mui/material/TextField';
import Register from './Register';

export default function Login() {
    const [open, setOpen] = React.useState(false);
    const [username, setUsername] = React.useState('');
    const [pass, setPass] = React.useState('');
    const [error, setError] = React.useState('');

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    async function onSubmit(e) {
        await fetch("http://localhost:5000/login", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({ name: username.toLowerCase(), password: pass }),
        })
            .then(res => res.json())
            .then(data => {
                // Check status
                if (data.status === "Logged In") {
                    localStorage.setItem("token", data.token);
                    window.location.reload();
                } else {
                    setError(data.status);
                }
            })
            .catch(error => {
                window.alert(error);
                return;
            });
    }

    return (
        <div>
            <Button variant="outlined" onClick={handleClickOpen}>
                Log in
            </Button>

            {/* Dialog that opens/hides when button is clicked */}
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">
                    Log in to your Account
                </DialogTitle>

                <DialogContent>
                    <DialogContentText>
                        <Register />
                        Enter your username and password to log in.
                    </DialogContentText>

                    <DialogContentText style={{color: "#FF0000"}}>
                        {error ? error : null}
                    </DialogContentText>

                    <TextField
                        autoFocus
                        margin="dense"
                        id="name"
                        label="Email Address/Name"
                        type="string"
                        fullWidth
                        variant="standard"
                        // Set react state as true value
                        onChange={(e) => {
                            setError(null);
                            setUsername(e.target.value)
                        }}
                        value={username}
                    />

                    <TextField
                        autoFocus
                        margin="dense"
                        id="pass"
                        label="Password"
                        type="password"
                        fullWidth
                        variant="standard"

                        // Set error to false so message disappears when they fix it
                        onChange={(e) => {
                            setError(null);
                            setPass(e.target.value)
                        }}
                        value={pass}
                    />
                </DialogContent>

                <DialogActions>
                    <Button onClick={handleClose}>Cancel</Button>
                    <Button onClick={onSubmit} autoFocus>
                        Sign In
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}