import * as React from 'react';

// MUI Components
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import TextField from '@mui/material/TextField';

import FileBase64 from 'react-file-base64';

export default function CreateBox() {
    const [open, setOpen] = React.useState(false);
    const [error, setError] = React.useState('');
    const [name, setName] = React.useState('');
    const [description, setDescription] = React.useState('');
    const [file, setFile] = React.useState();

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    async function onSubmit(e) {
        if (name === '') {
            setError('Name cannot be empty');
        }
        else {
            await fetch("http://localhost:5000/createBox", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    "x-access-token": localStorage.getItem("token")
                },
                body: JSON.stringify({ name: name, description: description, file: file }),
            })
                .then(res => res.json())
                .then(data => {
                    if (data.status === "Box Created") {
                        setOpen(false);
                    }
                })
                .catch(error => {
                    window.alert(error);
                    return;
                });
        }
    }

    return (
        <div>
            <Button variant="outlined" onClick={handleClickOpen}>
                Create Box
            </Button>

            {/* Dialog that opens/hides when button is clicked */}
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">
                    Create a new Box
                </DialogTitle>

                <DialogContent>
                    <DialogContentText>
                        Enter the details of the box you want to create.
                    </DialogContentText>

                    <DialogContentText style={{ color: "#FF0000" }}>
                        {error ? error : null}
                    </DialogContentText>

                    <TextField
                        autoFocus
                        margin="dense"
                        id="name"
                        label="Box Name"
                        type="string"
                        fullWidth
                        variant="standard"
                        // Set react state as true value
                        onChange={(e) => {
                            setError(null);
                            setName(e.target.value)
                        }}
                        value={name}
                    />

                    <TextField
                        autoFocus
                        margin="dense"
                        id="desc"
                        label="Description"
                        type="string"
                        fullWidth
                        variant="standard"

                        // Set error to false so message disappears when they fix it
                        onChange={(e) => {
                            setError(null);
                            setDescription(e.target.value)
                        }}
                        value={description}
                    />

                    <FileBase64 id="file-upload"
                        multiple={false}
                        onDone={(base64) => {
                            if (base64.name.split('.').pop() !== 'csv') {
                                setError('File must be a CSV');
                            }
                            else {
                                setError(null);
                                setFile(base64);
                            }
                        }} />
                </DialogContent>

                <DialogActions>
                    <Button onClick={handleClose}>Cancel</Button>
                    <Button onClick={onSubmit} autoFocus>
                        Create Box
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}
