import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import { Grid } from '@mui/material';

import DeleteBox from './DeleteBox';
import UpdateBox from './UpdateBox';
import ViewCSV from '../ViewCSV';

export default function OwnedBox(props) {
    // console.log(props)
    // return <h1>{props.name}</h1>
    return <Grid item xs={6} md={4}>
        <Card sx={{ maxWidth: 345 }} spacing={10}>
            <CardContent>
                <Typography gutterBottom variant="h5" component="div">
                    {props.name}
                </Typography>
                <Typography variant="body2" color="text.secondary">
                    {props.description}
                </Typography>

                {/* If there is a file, the user can view/download it */}
                {props.file_name ? <ViewCSV file_base64={props.file_base64} file_name={props.file_name} /> : "No File"}

            </CardContent>
            <CardActions>
                <DeleteBox {...props} />
                <UpdateBox {...props} />
            </CardActions>
        </Card>
    </Grid>
}