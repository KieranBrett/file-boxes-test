import * as React from 'react';

// MUI Components
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';

export default function DeleteBox(props) {
    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    async function onSubmit(e) {
        await fetch("http://localhost:5000/deleteBox", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "x-access-token": localStorage.getItem("token")
            },
            body: JSON.stringify({ id: props._id }),
        })
        .then(res => res.json())
        .then(data => { 
            if (data.status === "Box Deleted") {
                setOpen(false);
            }
        })
        .catch(error => {
            console.log("ooooops")
            window.alert(error);
            return;
        });
    }

    return (
        <div>
            <Button variant="outlined" color="error" onClick={handleClickOpen}>
                Delete
            </Button>

            {/* Dialog that opens/hides when button is clicked */}
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">
                    Delete Box
                </DialogTitle>

                <DialogContent>
                    <DialogContentText>
                        Are you sure you want to remove this box? This action cannot be undone.
                    </DialogContentText>
                </DialogContent>

                <DialogActions>
                    <Button onClick={handleClose}>Cancel</Button>
                    <Button onClick={onSubmit} autoFocus color="error">
                        Delete
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}