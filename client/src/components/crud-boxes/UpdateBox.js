import * as React from 'react';

// MUI Components
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import TextField from '@mui/material/TextField';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';

// For Uploading the file to the server (in base64 format)
import FileBase64 from 'react-file-base64';

export default function UpdateBox(props) {
    const [open, setOpen] = React.useState(false);
    const [error, setError] = React.useState('');
    const [name, setName] = React.useState(props.name);
    const [description, setDescription] = React.useState(props.description);
    const [file, setFile] = React.useState({
        name: props.file_name,
        base64: props.file_base64
    });
    const [visibility, setVisibility] = React.useState(props.public);
    const [sharedWith, setSharedWith] = React.useState(props.sharedWith ? props.sharedWith.join("\n") : ''); // Turn from array to string

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setError(null);
        setOpen(false);
    };

    async function onSubmit(e) {
        await fetch("http://localhost:5000/updateBox", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "x-access-token": localStorage.getItem("token")
            },
            body: JSON.stringify({ name: name, description: description, _id: props._id, public: visibility, file: file, sharedWith: sharedWith }),
        })
            .then(res => res.json())
            .then(data => {
                if (data.status === "Box Updated") {
                    setOpen(false);
                }
            })
            .catch(error => {
                window.alert(error);
                return;
            });
    }

    return (
        <div>
            <Button variant="outlined" onClick={handleClickOpen}>
                Edit
            </Button>
            {/* Dialog that opens/hides when button is clicked */}
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">
                    Edit your box: {props.name}
                </DialogTitle>

                <DialogContent>
                    <DialogContentText>
                        Enter the new details of the box.
                    </DialogContentText>

                    <DialogContentText style={{ color: "#FF0000" }}>
                        {error ? error : null}
                    </DialogContentText>
                    <FormControl>
                        <TextField
                            autoFocus
                            margin="dense"
                            id="name"
                            label="Box Name"
                            type="string"
                            fullWidth
                            variant="standard"
                            // Set react state as true value
                            onChange={(e) => {
                                setError(null);
                                setName(e.target.value)
                            }}
                            value={name}
                        />
                        <TextField
                            autoFocus
                            margin="dense"
                            id="desc"
                            label="Description"
                            type="string"
                            fullWidth
                            variant="standard"

                            // Set error to false so message disappears when they fix it
                            onChange={(e) => {
                                setError(null);
                                setDescription(e.target.value)
                            }}
                            value={description}
                        />

                        <FormLabel>File: {file.name}</FormLabel>
                        {file.name ?
                            <Button onClick={() => {
                                setFile('null');
                            }}>Remove File</Button>
                            :
                            <FileBase64 id="file-upload"
                                multiple={false}
                                onDone={(base64) => {
                                    if (base64.name.split('.').pop() !== 'csv') {
                                        setError('File must be a CSV');
                                    }
                                    else {
                                        setError(null);
                                        setFile(base64);
                                    }
                                }} />}
                                
                        <FormLabel>Visibile to Public</FormLabel>
                        <RadioGroup
                            row
                            name="public-radio"
                            value={visibility}
                            onChange={(e) => {
                                console.log("selected " + e.target.value);
                                // e.target.value returns value in a string (we need boolean)
                                setVisibility(e.target.value === "true" ? true : false);
                            }}
                        >
                            <FormControlLabel value={true} control={<Radio />} label="True" />
                            <FormControlLabel value={false} control={<Radio />} label="False" />
                        </RadioGroup>

                        <TextField
                            autoFocus
                            margin="dense"
                            id="shared"
                            label="Share with (Name or Email)"
                            type="string"
                            fullWidth
                            variant="standard"
                            multiline={true}
                            rows={3}
                            value={sharedWith}
                            // Set error to false so message disappears when they fix it
                            onChange={(e) => {
                                setSharedWith(e.target.value)
                            }}
                        />

                        <DialogContentText>
                            One user per line.
                        </DialogContentText>
                    </FormControl>
                </DialogContent>

                <DialogActions>
                    <Button onClick={handleClose}>Cancel</Button>
                    <Button onClick={onSubmit} autoFocus>
                        Update Box
                    </Button>
                </DialogActions>
            </Dialog>
        </div >
    );
}