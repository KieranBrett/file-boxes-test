import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import { Grid } from '@mui/material';

import ViewCSV from '../ViewCSV';

// Public box and Owned box could use a class template,
// But there may be future features that require them to be seperate
export default function PublicBox(props) {
    return <Grid item xs={6} md={4}>
        <Card sx={{ maxWidth: 345 }}>
            <CardContent>
                <Typography gutterBottom variant="h5" component="div">
                    {props.name}
                </Typography>
                <Typography gutterBottom variant="body1" component="div">
                    Owner ID: {props.owner_id}
                </Typography>
                <Typography variant="body2" color="text.secondary">
                    {props.description}
                </Typography>
                <Typography variant="body2" color="text.secondary">
                    {props.file_name ? <ViewCSV file_base64={props.file_base64} file_name={props.file_name} /> : "No File"}
                </Typography>
            </CardContent>
        </Card>
    </Grid>
}