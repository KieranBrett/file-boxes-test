# File-Boxes-Test
Web application for managing CSV files. Create a box and upload a CSV File to it. You can share your box with the public, a specific user, or just keep the box private. Preview your files online before downloading. There are also Superuser accounts that can see/delete everything.

![](https://i.imgur.com/gldCwFT.png)

## Launching the web app

### Hosting the server (middleware)
Open a command prompt to the file-boxes-test/server directory 
```
~file-boxes-test/server>
```
Install all of the required dependancies using npm
```
~file-boxes-test/server>npm install
```
After installing, you can start the server using node
```
~file-boxes-test/client>node server.js
```
You should see the following
```
Server is running on port: 5000
Successfully connected to MongoDB.
```

### Hosting the client
Open a new command prompt to the file-boxes-test/client directory 
```
~file-boxes-test/client>
```
Install all of the required dependancies using npm
```
~file-boxes-test/client>npm install
```
After installing, you can start the client
```
~file-boxes-test/client>npm start
```
The web app should open in your browser

## MongoDB Atlas
- [MongoDB Atlas Sign In](https://account.mongodb.com/account/login)

## Helpful resources

- [MongoDB & Node.js](https://www.mongodb.com/blog/post/quick-start-nodejs-mongodb-how-to-get-connected-to-your-database)
- [MongoDB MERN Stack](https://www.mongodb.com/languages/mern-stack-tutorial)
- [MongoDB CRUD](https://docs.mongodb.com/manual/crud/)
- [MUI](https://mui.com/)
- [JWT Best Practices](https://blog.openreplay.com/jwt-authentication-best-practices)

## Project status
Mostly Finished (Some more styling to be finished)
